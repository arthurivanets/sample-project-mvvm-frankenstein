package com.arthurivanets.sample

import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.multidex.MultiDexApplication
import com.arthurivanets.sample.di.components.AppDependenciesComponent
import com.arthurivanets.sample.di.components.DaggerAppDependenciesComponent
import com.arthurivanets.sample.di.util.AndroidXHasSupportFragmentInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class SampleApplication : MultiDexApplication(), HasActivityInjector, AndroidXHasSupportFragmentInjector {


    lateinit var dependenciesComponent : AppDependenciesComponent
        private set

    @Inject
    lateinit var activityDispatchingAndroidInjector : DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var fragmentDispatchingAndroidInjector : DispatchingAndroidInjector<Fragment>


    companion object {

        @JvmStatic lateinit var INSTANCE : SampleApplication
            private set

    }


    override fun onCreate() {
        super.onCreate()

        INSTANCE = this

        initDagger()
    }


    private fun initDagger() {
        dependenciesComponent = DaggerAppDependenciesComponent.builder()
            .application(this)
            .build()

        dependenciesComponent.inject(this)
    }


    override fun activityInjector() : AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }


    override fun supportFragmentInjector() : AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }


}