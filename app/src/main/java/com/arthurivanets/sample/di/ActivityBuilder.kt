package com.arthurivanets.sample.di

import com.arthurivanets.sample.di.modules.activities.MainActivityModule
import com.arthurivanets.sample.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {


    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity() : MainActivity


}