package com.arthurivanets.sample.di.components

import android.app.Application
import com.arthurivanets.sample.di.ActivityBuilder
import com.arthurivanets.sample.di.modules.general.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AndroidSupportInjectionModule::class,
    AppModule::class,
    ActivityBuilder::class
])
interface AppDependenciesComponent : Injectables {


    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application : Application) : Builder

        fun build() : AppDependenciesComponent

    }


}