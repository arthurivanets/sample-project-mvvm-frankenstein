package com.arthurivanets.sample.di.components

import com.arthurivanets.sample.SampleApplication

interface Injectables {

    fun inject(application : SampleApplication)

}