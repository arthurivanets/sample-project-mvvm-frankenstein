package com.arthurivanets.sample.di.modules.activities

import androidx.lifecycle.ViewModelProviders
import com.arthurivanets.sample.ui.base.ViewModelProviderFactory
import com.arthurivanets.sample.ui.main.MainActivity
import com.arthurivanets.sample.ui.main.MainActivityViewModel
import com.arthurivanets.sample.ui.main.MainActivityViewModelImpl
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {


    @Provides
    fun provideMainViewModel(activity : MainActivity) : MainActivityViewModel {
        val viewModelFactory = ViewModelProviderFactory(MainActivityViewModelImpl())
        return ViewModelProviders.of(activity, viewModelFactory).get(MainActivityViewModelImpl::class.java)
    }


}