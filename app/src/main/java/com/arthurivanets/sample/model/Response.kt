package com.arthurivanets.sample.model

data class Response<ResultType, ErrorType>(
    val result : ResultType? = null,
    val error : ErrorType? = null
) {


    val isErroneous : Boolean
        get() = hasError

    val hasResult : Boolean
        get() = (result != null)

    val hasError : Boolean
        get() = (error != null)


}