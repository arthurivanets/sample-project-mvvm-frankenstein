package com.arthurivanets.sample.model

import com.arthurivanets.sample.model.Response
import com.arthurivanets.sample.util.exceptions.HttpResponseError
import com.arthurivanets.sample.util.exceptions.ResponseError
import com.arthurivanets.sample.util.extensions.errorOrDefault

object Responses {


    /**
     *
     */
    @JvmStatic fun <T> result(result : T?) : Response<T, Throwable> {
        return Response(result = result)
    }


    /**
     * //TODO <---
     */
    @JvmStatic fun <T> errorOrNullResponse(erroneousResponse : Response<*, Throwable>) : Response<T, Throwable> {
        return Response(error = (if (!erroneousResponse.isErroneous && !erroneousResponse.hasResult) ResponseError("Response is null.") else erroneousResponse.errorOrDefault()))
    }


    /**
     *
     */
    @JvmStatic fun <T> errorResponse(erroneousResponse : Response<*, Throwable>) : Response<T, Throwable> {
        return Response(error = erroneousResponse.errorOrDefault())
    }


    /**
     * Creates an erroneous response with the specified (optional) message.
     */
    @JvmStatic fun <T> errorResponse(errorMessage : String = "") : Response<T, Throwable> {
        return errorResponse(ResponseError(errorMessage))
    }


    /**
     *
     */
    @JvmStatic fun <T> errorResponse(error : Throwable) : Response<T, Throwable> {
        return Response(error = error)
    }


    /**
     * Creates an erroneous response for the occurred Http Response Error.
     */
    @JvmStatic fun <T> httpErrorResponse(responseCode : Int, statusMessage : String? = "") : Response<T, Throwable> {
        return Response(error = HttpResponseError(
            responseCode = responseCode,
            statusMessage = (statusMessage ?: "")
        ))
    }


}