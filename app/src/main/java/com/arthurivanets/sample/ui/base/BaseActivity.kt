package com.arthurivanets.sample.ui.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.arthurivanets.sample.di.util.AndroidXHasSupportFragmentInjector
import com.arthurivanets.sample.ui.util.events.GeneralViewModelEvents
import com.arthurivanets.sample.ui.util.events.ViewEvent
import com.arthurivanets.sample.ui.util.events.ViewModelEvent
import com.arthurivanets.sample.ui.util.events.consume
import com.arthurivanets.sample.util.eventbus.BusEvent
import com.arthurivanets.sample.util.eventbus.RxBusImpl
import com.arthurivanets.sample.util.extensions.handleBackPressEvent
import com.arthurivanets.sample.util.extensions.longToast
import com.arthurivanets.sample.util.extensions.shortToast
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import javax.inject.Inject

/**
 *
 */
abstract class BaseActivity<VDB : ViewDataBinding, VM : BaseViewModel> : AppCompatActivity(), AndroidXHasSupportFragmentInjector {


    @Inject
    lateinit var fragmentDispatchingAndroidInjector : DispatchingAndroidInjector<Fragment>

    private var viewDataBinding : VDB? = null
    private var viewModel : VM? = null
    private var eventConsumerDisposable : Disposable? = null


    final override fun onCreate(savedInstanceState : Bundle?) {
        injectDependencies()
        savedInstanceState?.let(::fetchExtras)
        preInit()
        super.onCreate(savedInstanceState)
        performDataBinding()
        init(savedInstanceState)
        postInit()
    }


    /**
     *
     */
    open fun injectDependencies() {
        AndroidInjection.inject(this)
    }


    /**
     * Gets called right before the pre-initialization stage ([preInit] method call),
     * if the [Bundle] received from the [onCreate] is not null.
     */
    protected open fun fetchExtras(extras : Bundle) {
        //
    }


    /**
     * Gets called right before the UI initialization.
     */
    protected open fun preInit() {
        //
    }


    /**
     * Here you should initialize all your UI Elements.
     */
    protected open fun init(savedInstanceState : Bundle?) {
        //
    }


    /**
     * Gets called after the UI initialization completed.
     */
    protected open fun postInit() {
        //
    }


    open fun performDataBinding() {
        viewDataBinding = (viewDataBinding ?: DataBindingUtil.setContentView(this, getLayoutId()))
        viewModel = (viewModel ?: getViewModel())

        viewDataBinding?.setVariable(getBindingVariable(), viewModel)
        viewDataBinding?.executePendingBindings()
    }


    @CallSuper
    override fun onResume() {
        super.onResume()

        subscribeEventConsumer()
    }


    @CallSuper
    override fun onPause() {
        super.onPause()

        unsubscribeEventConsumer()
    }


    @CallSuper
    override fun onBackPressed() {
        val isBackPressEventConsumed = (viewModel?.onBackPressed() ?: false)

        if(!isBackPressEventConsumed && !handleBackPressEvent()) {
            super.onBackPressed()
        }
    }


    private fun handleBackPressEvent() : Boolean {
        return supportFragmentManager.fragments.handleBackPressEvent()
    }


    final override fun onDestroy() {
        onRecycle()
        super.onDestroy()
    }


    protected open fun onRecycle() {
        // to be overridden
    }


    @CallSuper
    protected open fun onEvent(event : ViewModelEvent<*>) {
        when(event) {
            is GeneralViewModelEvents.ShortToast -> event.consume { data?.let(::shortToast) }
            is GeneralViewModelEvents.LongToast -> event.consume { data?.let(::longToast) }
            is GeneralViewModelEvents.ConfirmBackButtonPress -> onBackPressed()
            is GeneralViewModelEvents.StartActivity -> startActivity(event.getDataAs())
            is GeneralViewModelEvents.FinishActivity -> finish()
        }
    }


    protected fun dispatchViewEvent(event : ViewEvent<*>) {
        viewModel?.dispatchViewEvent(event)
    }


    protected fun dispatchBusEvent(event : BusEvent<*>) {
        RxBusImpl.INSTANCE.post(event)
    }


    private fun subscribeEventConsumer() {
        eventConsumerDisposable = RxBusImpl.INSTANCE
            .registerOnUiThread(ViewModelEvent::class.java, Consumer(::onEvent))
    }


    private fun unsubscribeEventConsumer() {
        eventConsumerDisposable?.dispose()
        eventConsumerDisposable = null
    }


    override fun supportFragmentInjector() : AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }


    /**
     *
     */
    @LayoutRes
    abstract fun getLayoutId() : Int


    /**
     *
     */
    abstract fun getBindingVariable() : Int


    /**
     *
     */
    abstract fun getViewModel() : VM


}