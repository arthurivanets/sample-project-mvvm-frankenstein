package com.arthurivanets.sample.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.arthurivanets.sample.di.util.AndroidXSupportInjection
import com.arthurivanets.sample.ui.markers.CanHandleBackPressEvents
import com.arthurivanets.sample.ui.util.events.GeneralViewModelEvents
import com.arthurivanets.sample.ui.util.events.ViewEvent
import com.arthurivanets.sample.ui.util.events.consume
import com.arthurivanets.sample.util.eventbus.BusEvent
import com.arthurivanets.sample.util.eventbus.RxBusImpl
import com.arthurivanets.sample.util.extensions.longToast
import com.arthurivanets.sample.util.extensions.shortToast
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

abstract class BaseFragment<VDB : ViewDataBinding, VM : BaseViewModel> : Fragment(), CanHandleBackPressEvents {


    lateinit var rootView : View
        private set

    private var viewDataBinding : VDB? = null
    private var viewModel : VM? = null
    private var eventConsumerDisposable : Disposable? = null

    var isViewCreated = false
        private set


    final override fun onCreate(savedInstanceState : Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        initViewModel()
        savedInstanceState?.let(::fetchExtras)
        preInit()
    }


    override fun onCreateView(inflater : LayoutInflater, container : ViewGroup?, savedInstanceState : Bundle?) : View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        viewDataBinding?.let { rootView = it.root }

        isViewCreated = true

        return rootView
    }


    final override fun onViewCreated(view : View, savedInstanceState : Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init(savedInstanceState)
        postInit()
    }


    /**
     *
     */
    open fun injectDependencies() {
        AndroidXSupportInjection.inject(this)
    }


    /**
     * Gets called right before the pre-initialization stage ([preInit] method call),
     * if the [Bundle] received from the [onViewCreated] is not null.
     */
    protected open fun fetchExtras(extras : Bundle) {
        //
    }


    /**
     * Gets called right before the UI initialization.
     */
    protected open fun preInit() {
        //
    }


    private fun initViewModel() {
        viewModel = getViewModel()
    }


    /**
     * Here you should initialize all your UI Elements.
     */
    protected open fun init(savedInstanceState : Bundle?) {
        //
    }


    /**
     * Gets called after the UI initialization completed.
     */
    protected open fun postInit() {
        //
    }


    @CallSuper
    override fun onResume() {
        super.onResume()

        subscribeEventConsumer()
    }


    @CallSuper
    override fun onPause() {
        super.onPause()

        unsubscribeEventConsumer()
    }


    @CallSuper
    override fun onBackPressed() : Boolean {
        return (viewModel?.onBackPressed() ?: false)
    }


    final override fun onDestroy() {
        onRecycle()
        super.onDestroy()
    }


    protected open fun onRecycle() {
        //
    }


    @CallSuper
    protected open fun onEvent(event : BusEvent<*>) {
        when(event) {
            is GeneralViewModelEvents.ShortToast -> event.consume { data?.let { context?.shortToast(it) } }
            is GeneralViewModelEvents.LongToast -> event.consume { data?.let { context?.longToast(it) } }
            is GeneralViewModelEvents.StartActivity -> startActivity(event.getDataAs())
            is GeneralViewModelEvents.FinishActivity -> activity?.finish()
        }
    }


    /**
     *
     */
    protected open fun onBecameVisibleToUser() {
        //
    }


    /**
     *
     */
    protected open fun onBecameInvisibleToUser() {
        //
    }


    protected fun dispatchViewEvent(event : ViewEvent<*>) {
        viewModel?.dispatchViewEvent(event)
    }


    protected fun dispatchBusEvent(event : BusEvent<*>) {
        RxBusImpl.INSTANCE.post(event)
    }


    private fun subscribeEventConsumer() {
        eventConsumerDisposable = RxBusImpl.INSTANCE
            .registerOnUiThread(BusEvent::class.java, Consumer(::onEvent))
    }


    private fun unsubscribeEventConsumer() {
        eventConsumerDisposable?.dispose()
        eventConsumerDisposable = null
    }


    final override fun setUserVisibleHint(isVisibleToUser : Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        if(isVisibleToUser) {
            onBecameVisibleToUser()
        } else {
            onBecameInvisibleToUser()
        }
    }


    /**
     *
     */
    @LayoutRes
    abstract fun getLayoutId() : Int


    /**
     *
     */
    abstract fun getBindingVariable() : Int


    /**
     *
     */
    abstract fun getViewModel() : VM


}