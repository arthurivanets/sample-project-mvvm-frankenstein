package com.arthurivanets.sample.ui.base

import androidx.databinding.ObservableBoolean
import com.arthurivanets.sample.ui.markers.CanHandleBackPressEvents
import com.arthurivanets.sample.ui.util.events.ViewEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

interface BaseViewModel : CanHandleBackPressEvents {

    fun dispatchViewEvent(event : ViewEvent<*>)

    fun addDisposable(disposable : Disposable)

    fun getDisposables() : CompositeDisposable

    fun setLoading(isLoading : Boolean)

    fun isLoading() : ObservableBoolean

}