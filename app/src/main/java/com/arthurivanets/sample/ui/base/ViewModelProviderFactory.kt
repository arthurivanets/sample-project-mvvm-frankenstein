package com.arthurivanets.sample.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelProviderFactory<V : ViewModel>(private val viewModel : V) : ViewModelProvider.Factory {


    @SuppressWarnings("unchecked")
    override fun <T : ViewModel?> create(modelClass : Class<T>) : T {
        if(!modelClass.isAssignableFrom(viewModel::class.java)) {
            throw IllegalArgumentException("Unsupported class name.")
        }

        return (viewModel as T)
    }


}