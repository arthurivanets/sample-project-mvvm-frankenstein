package com.arthurivanets.sample.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.arthurivanets.sample.BR
import com.arthurivanets.sample.R
import com.arthurivanets.sample.databinding.ActivityMainBinding
import com.arthurivanets.sample.ui.base.BaseActivity
import com.arthurivanets.sample.ui.util.events.ViewModelEvent
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity<ActivityMainBinding, MainActivityViewModel>() {


    @Inject
    lateinit var localViewModel : MainActivityViewModel


    companion object {

        const val TAG = "MainActivity"

        @JvmStatic fun newInstance(context : Context) : Intent {
            return Intent(context, MainActivity::class.java)
        }

    }


    override fun init(savedInstanceState : Bundle?) {
        initInfoButton()
        initAButton()
        initBButton()
        initClearButton()
    }


    private fun initInfoButton() {
        infoBtn.setOnClickListener { dispatchViewEvent(MainActivityViewEvents.InfoActivityButtonClicked()) }
    }


    private fun initAButton() {
        aBtn.setOnClickListener { dispatchViewEvent(MainActivityViewEvents.ButtonAClicked(aBtn.text.toString())) }
    }


    private fun initBButton() {
        bBtn.setOnClickListener { dispatchViewEvent(MainActivityViewEvents.ButtonBClicked(bBtn.text.toString())) }
    }


    private fun initClearButton() {
        clearBtn.setOnClickListener { dispatchViewEvent(MainActivityViewEvents.ClearButtonClicked()) }
    }


    private fun appendLogMessage(message : String) {
        logTv.append("\n$message")
    }


    private fun clearLog() {
        logTv.text = ""
    }


    private fun launchInfoActivity() {
        //TODO <---
    }


    override fun onEvent(event : ViewModelEvent<*>) {
        super.onEvent(event)

        when(event) {
            is MainActivityViewModelEvents.OpenInfoScreen -> launchInfoActivity()
            is MainActivityViewModelEvents.LogEvent -> event.data?.let(::appendLogMessage)
            is MainActivityViewModelEvents.ClearLog -> clearLog()
        }
    }


    override fun getLayoutId() : Int {
        return R.layout.activity_main
    }


    override fun getBindingVariable() : Int {
        return BR.viewModel
    }


    override fun getViewModel() : MainActivityViewModel {
        return localViewModel
    }


}
