package com.arthurivanets.sample.ui.main

import com.arthurivanets.sample.ui.util.events.ViewEvent

sealed class MainActivityViewEvents<T>(data : T? = null) : ViewEvent<T>(data) {

    class InfoActivityButtonClicked : MainActivityViewEvents<Unit>()

    class ButtonAClicked(title : String) : MainActivityViewEvents<String>(title)

    class ButtonBClicked(title : String) : MainActivityViewEvents<String>(title)

    class ClearButtonClicked : MainActivityViewEvents<Unit>()

}