package com.arthurivanets.sample.ui.main

import com.arthurivanets.sample.ui.util.events.ViewModelEvent

sealed class MainActivityViewModelEvents<T>(data : T? = null) : ViewModelEvent<T>(data) {

    class OpenInfoScreen : MainActivityViewModelEvents<Unit>()

    class LogEvent(message : String) : MainActivityViewModelEvents<String>(message)

    class ClearLog : MainActivityViewModelEvents<Unit>()

}