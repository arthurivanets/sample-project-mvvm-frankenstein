package com.arthurivanets.sample.ui.main

import com.arthurivanets.sample.ui.base.BaseViewModelImpl
import com.arthurivanets.sample.ui.util.events.GeneralViewModelEvents
import com.arthurivanets.sample.util.eventbus.BusEvent

class MainActivityViewModelImpl : BaseViewModelImpl(), MainActivityViewModel {


    override fun onEvent(event : BusEvent<*>) {
        super.onEvent(event)

        when(event) {
            is MainActivityViewEvents.InfoActivityButtonClicked -> onInfoActivityButtonClicked()
            is MainActivityViewEvents.ButtonAClicked -> event.data?.let(::onButtonAClicked)
            is MainActivityViewEvents.ButtonBClicked -> event.data?.let(::onButtonBClicked)
            is MainActivityViewEvents.ClearButtonClicked -> onClearButtonClicked()
        }
    }


    private fun onInfoActivityButtonClicked() {
        dispatchEvent(GeneralViewModelEvents.ShortToast("Info Activity Button Clicked."))
        dispatchEvent(MainActivityViewModelEvents.OpenInfoScreen())
    }


    private fun onButtonAClicked(title : String) {
        dispatchEvent(GeneralViewModelEvents.ShortToast("Button A Clicked."))
        dispatchEvent(MainActivityViewModelEvents.LogEvent("[$title] -> Clicked."))
    }


    private fun onButtonBClicked(title : String) {
        dispatchEvent(GeneralViewModelEvents.ShortToast("Button B Clicked."))
        dispatchEvent(MainActivityViewModelEvents.LogEvent("[$title] -> Clicked."))
    }


    private fun onClearButtonClicked() {
        dispatchEvent(GeneralViewModelEvents.ShortToast("Clear Button Clicked."))
        dispatchEvent(MainActivityViewModelEvents.ClearLog())
    }


}