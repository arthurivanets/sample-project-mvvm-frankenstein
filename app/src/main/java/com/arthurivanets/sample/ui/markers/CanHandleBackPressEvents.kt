package com.arthurivanets.sample.ui.markers

interface CanHandleBackPressEvents {

    /**
     * @return <strong>true</strong> if the back press event has been consumed, <strong>false</strong> otherwise.
     */
    fun onBackPressed() : Boolean

}