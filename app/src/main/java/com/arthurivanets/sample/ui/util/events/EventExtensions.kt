@file:JvmName("EventExtensions")

package com.arthurivanets.sample.ui.util.events

import com.arthurivanets.sample.util.eventbus.BusEvent


/**
 * Consumes the Event if necessary (if the Event isn't consumed already).
 */
inline fun <T> BusEvent<T>.consume(consumer : BusEvent<T>.() -> Unit) {
    if(!this.isConsumed) {
        consumer(this)
        this.consume()
    }
}

