package com.arthurivanets.sample.ui.util.events

sealed class GeneralViewEvents<T>(data : T? = null) : ViewEvent<T>(data) {

    class OnPreInit : GeneralViewEvents<Unit>()

    class BackButtonPressed : GeneralViewEvents<Unit>()

    class LoadData : GeneralViewEvents<Unit>()

    class CancelDataLoading : GeneralViewEvents<Unit>()

}