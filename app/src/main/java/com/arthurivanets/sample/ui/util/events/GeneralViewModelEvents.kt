package com.arthurivanets.sample.ui.util.events

import android.content.Intent

sealed class GeneralViewModelEvents<T>(data : T? = null) : ViewModelEvent<T>(data) {

    class ShortToast(message : String) : GeneralViewModelEvents<String>(message)

    class LongToast(message : String) : GeneralViewModelEvents<String>(message)

    class ShowRecyclerView(animate : Boolean = true) : GeneralViewModelEvents<Boolean>(animate)

    class HideRecyclerView(animate : Boolean = true) : GeneralViewModelEvents<Boolean>(animate)

    class ConfirmBackButtonPress : GeneralViewModelEvents<Unit>()

    class StartActivity(intent : Intent) : GeneralViewModelEvents<Intent>(intent)

    class FinishActivity : GeneralViewModelEvents<Unit>()

}