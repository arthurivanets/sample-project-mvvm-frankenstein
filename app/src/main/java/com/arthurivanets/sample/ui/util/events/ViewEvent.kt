package com.arthurivanets.sample.ui.util.events

import com.arthurivanets.sample.util.eventbus.BusEvent

open class ViewEvent<T>(data : T? = null) : BusEvent<T>(data)