package com.arthurivanets.sample.ui.util.events

import com.arthurivanets.sample.util.eventbus.BusEvent

open class ViewModelEvent<T>(data : T? = null) : BusEvent<T>(data)