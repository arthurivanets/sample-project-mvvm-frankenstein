package com.arthurivanets.sample.util.eventbus

abstract class BusEvent<T>(val data : T? = null) {


    var isConsumed : Boolean = false
        private set

    val hasData : Boolean
        get() = (data != null)


    fun consume() {
        isConsumed = true
    }


    fun <DT> getDataAs() : DT? {
        return (data as DT?)
    }


}