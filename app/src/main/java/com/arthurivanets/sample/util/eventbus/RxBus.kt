package com.arthurivanets.sample.util.eventbus

import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

interface RxBus {

    fun <T> register(eventType : Class<T>, onEvent : Consumer<T>) : Disposable

    fun <T> registerOnUiThread(eventType : Class<T>, onEvent : Consumer<T>) : Disposable

    fun post(event : Any)

}