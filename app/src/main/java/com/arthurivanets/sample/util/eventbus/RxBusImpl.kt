package com.arthurivanets.sample.util.eventbus

import com.arthurivanets.sample.util.extensions.observeOnUI
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject

class RxBusImpl : RxBus {


    private val busSubject = PublishSubject.create<Any>().toSerialized()


    companion object {

        @JvmStatic val INSTANCE : RxBus = RxBusImpl()

    }


    override fun <T> register(eventType : Class<T>, onEvent : Consumer<T>) : Disposable {
        return busSubject.filter { eventType.isInstance(it) }
            .map { (it as T) }
            .subscribe(onEvent)
    }


    override fun <T> registerOnUiThread(eventType : Class<T>, onEvent : Consumer<T>) : Disposable {
        return busSubject.filter { eventType.isInstance(it) }
            .map { (it as T) }
            .observeOnUI()
            .subscribe(onEvent)
    }


    override fun post(event : Any) {
        busSubject.onNext(event)
    }


}