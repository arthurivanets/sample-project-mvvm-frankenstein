package com.arthurivanets.sample.util.exceptions

open class HttpResponseError(
    val responseCode : Int,
    val statusMessage : String = ""
) : ResponseError("Http Response resulted in Error. Response Code $responseCode, Status Message $statusMessage")