package com.arthurivanets.sample.util.exceptions

open class ResponseError(message : String = "") : RuntimeException(message)