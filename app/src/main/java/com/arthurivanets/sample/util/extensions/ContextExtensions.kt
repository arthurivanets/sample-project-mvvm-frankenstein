@file:JvmName("ContextExtensions")

package com.arthurivanets.sample.util.extensions

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat


/**
 *
 */
fun Context.newLayoutInflater() : LayoutInflater {
    return LayoutInflater.from(this)
}


/**
 *
 */
fun Context.inflateView(@LayoutRes layoutResourceId : Int,
                        root : ViewGroup?,
                        attachToRoot : Boolean = true) : View {
    return newLayoutInflater().inflate(
        layoutResourceId,
        root,
        attachToRoot
    )
}


/**
 *
 */
fun Context.showKeyboard(view : View) {
    (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?)?.showSoftInput(view, 0)
}


/**
 *
 */
fun Context.hideKeyboard(view : View) {
    (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?)?.hideSoftInputFromWindow(view.windowToken, 0)
}


/**
 *
 */
fun Context.getColorCompat(@ColorRes colorResourceId : Int) : Int {
    return ContextCompat.getColor(this, colorResourceId)
}


/**
 *
 */
fun Context.getDimension(@DimenRes dimensionResourceId : Int) : Float {
    return this.resources.getDimension(dimensionResourceId)
}


/**
 *
 */
fun Context.getDimensionPixelSize(@DimenRes dimensionResourceId : Int) : Int {
    return this.resources.getDimensionPixelSize(dimensionResourceId)
}


/**
 *
 */
@SuppressLint("Recycle")
fun Context.extractStyledAttributes(attributes : AttributeSet,
                                    attrs : IntArray,
                                    block : TypedArray.() -> Unit) {
    obtainStyledAttributes(attributes, attrs, 0, 0)?.let {
        block(it)
        it.recycle()
    }
}


/**
 *
 */
fun Context.shortToast(message : String) {
    toast(message, Toast.LENGTH_SHORT)
}


/**
 *
 */
fun Context.longToast(message : String) {
    toast(message, Toast.LENGTH_LONG)
}


/**
 *
 */
fun Context.toast(message : String, duration : Int = Toast.LENGTH_SHORT) {
    Toast.makeText(
        this,
        message,
        duration
    ).show()
}
