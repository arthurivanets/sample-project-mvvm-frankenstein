@file:JvmName("FragmentManagerExtensions")

package com.arthurivanets.sample.util.extensions

import androidx.fragment.app.Fragment
import com.arthurivanets.sample.ui.markers.CanHandleBackPressEvents


/**
 *
 */
fun Collection<Fragment>.handleBackPressEvent() : Boolean {
    for(fragment in this) {
        if((fragment is CanHandleBackPressEvents) && fragment.onBackPressed()) {
            return true
        }
    }

    return false
}