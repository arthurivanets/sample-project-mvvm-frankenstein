@file:JvmName("ResponseExtensions")

package com.arthurivanets.sample.util.extensions

import com.arthurivanets.sample.model.Response
import com.arthurivanets.sample.model.Responses
import com.arthurivanets.sample.util.exceptions.ResponseError

/**
 *
 */
inline fun <T> resultOrError(block : () -> T?) : Response<T, Throwable> {
    var result : T? = null
    var error : Throwable? = null

    try {
        result = block()
    } catch(throwable : Throwable) {
        error = throwable
    }

    return Response(
        result = result,
        error = error
    )
}


/**
 *
 */
fun <T> T.asResult() : Response<T, Throwable> {
    return Responses.result(this)
}


/**
 *
 */
fun <R, E : Throwable> E.asError() : Response<R, Throwable> {
    return Responses.errorResponse(this)
}


/**
 *
 */
fun <T> Response<T, Throwable>.asError() : Response<T, Throwable> {
    return Responses.errorResponse(this)
}


/**
 *
 */
fun <T> Response<T, Throwable>.asErrorOrNullResponse() : Response<T, Throwable> {
    return Responses.errorOrNullResponse(this)
}


/**
 *
 */
fun Response<*, Throwable>.errorOrDefault() : Throwable {
    return (this.error ?: ResponseError())
}


/**
 *
 */
fun Response<*, Throwable>.isErroneousOrNullResponse() : Boolean {
    return (this.isErroneous || !this.hasResult)
}


/**
 *
 */
fun <T : Collection<*>> Response<T, Throwable>.isEmptyResponse() : Boolean {
    return (this.result?.isEmpty() ?: true)
}
