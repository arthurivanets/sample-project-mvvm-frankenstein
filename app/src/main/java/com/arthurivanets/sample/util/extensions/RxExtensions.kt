@file:JvmName("RxExtensions")

package com.arthurivanets.sample.util.extensions

import com.arthurivanets.sample.model.Response
import com.arthurivanets.sample.util.exceptions.ResponseError
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 *
 */
fun <T> Observable<Response<T, Throwable>>.resultOrError() : Observable<T> {
    return this.flatMapOrError { Observable.just(it) }
}


/**
 *
 */
fun <T> Flowable<Response<T, Throwable>>.resultOrError() : Flowable<T> {
    return this.flatMapOrError { Flowable.just(it) }
}


/**
 *
 */
fun <T> Single<Response<T, Throwable>>.resultOrError() : Single<T> {
    return this.flatMapOrError { Single.just(it) }
}


/**
 *
 */
fun <T> Maybe<Response<T, Throwable>>.resultOrError() : Maybe<T> {
    return this.flatMapOrError { Maybe.just(it) }
}


/**
 *
 */
fun <T> Observable<Response<T, Throwable>>.successfulResponseOrError() : Observable<Response<T, Throwable>> {
    return this.flatMapResponseOrErrorOut { Observable.just(it) }
}


/**
 *
 */
fun <T> Flowable<Response<T, Throwable>>.successfulResponseOrError() : Flowable<Response<T, Throwable>> {
    return this.flatMapResponseOrErrorOut { Flowable.just(it) }
}


/**
 *
 */
fun <T> Single<Response<T, Throwable>>.successfulResponseOrError() : Single<Response<T, Throwable>> {
    return this.flatMapResponseOrErrorOut { Single.just(it) }
}


/**
 *
 */
fun <T> Maybe<Response<T, Throwable>>.successfulResponseOrError() : Maybe<Response<T, Throwable>> {
    return this.flatMapResponseOrErrorOut { Maybe.just(it) }
}


/**
 *
 */
inline fun <T> Observable<T>.also(crossinline action : (T) -> Unit) : Observable<T> {
    return this.flatMap {
        action(it)
        return@flatMap it.asObservable()
    }
}


/**
 *
 */
inline fun <T> Flowable<T>.also(crossinline action : (T) -> Unit) : Flowable<T> {
    return this.flatMap {
        action(it)
        return@flatMap it.asFlowable()
    }
}


/**
 *
 */
inline fun <T> Single<T>.also(crossinline action : (T) -> Unit) : Single<T> {
    return this.flatMap {
        action(it)
        return@flatMap it.asSingle()
    }
}


/**
 *
 */
inline fun <T> Maybe<T>.also(crossinline action : (T) -> Unit) : Maybe<T> {
    return this.flatMap {
        action(it)
        return@flatMap it.asMaybe()
    }
}


/**
 *
 */
inline fun <T, A, R> Observable<Response<T, Throwable>>.flatMapOrError(argument : A, crossinline mappingFunction : (A, T) -> Observable<R>) : Observable<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Observable.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Observable.error(ResponseError("The Response Result is null."))
        } else {
            mappingFunction(argument, it.result!!)
        }
    }
}


/**
 *
 */
inline fun <T, A, R> Flowable<Response<T, Throwable>>.flatMapWithArgumentOrError(argument : A, crossinline mappingFunction : (A, T) -> Flowable<R>) : Flowable<R> {
    return this.flatMapOrError { mappingFunction(argument, it) }
}


/**
 *
 */
inline fun <T, A, R> Single<Response<T, Throwable>>.flatMapWithArgumentOrError(argument : A, crossinline mappingFunction : (A, T) -> Single<R>) : Single<R> {
    return this.flatMapOrError { mappingFunction(argument, it) }
}


/**
 *
 */
inline fun <T, A, R> Maybe<Response<T, Throwable>>.flatMapWithArgumentOrError(argument : A, crossinline mappingFunction : (A, T) -> Maybe<R>) : Maybe<R> {
    return this.flatMapOrError { mappingFunction(argument, it) }
}


/**
 *
 */
inline fun <T, R> Observable<Response<T, Throwable>>.flatMapOrError(crossinline mappingFunction : (T) -> Observable<R>) : Observable<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Observable.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Observable.error(ResponseError("The Response Result is null."))
        } else {
            mappingFunction(it.result!!)
        }
    }
}


/**
 *
 */
inline fun <T, R> Flowable<Response<T, Throwable>>.flatMapOrError(crossinline mappingFunction : (T) -> Flowable<R>) : Flowable<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Flowable.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Flowable.error(ResponseError("The Response Result is null."))
        } else {
            mappingFunction(it.result!!)
        }
    }
}


/**
 *
 */
inline fun <T, R> Single<Response<T, Throwable>>.flatMapOrError(crossinline mappingFunction : (T) -> Single<R>) : Single<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Single.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Single.error(ResponseError("The Response Result is null."))
        } else {
            mappingFunction(it.result!!)
        }
    }
}


/**
 *
 */
inline fun <T, R> Maybe<Response<T, Throwable>>.flatMapOrError(crossinline mappingFunction : (T) -> Maybe<R>) : Maybe<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Maybe.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Maybe.error(ResponseError("The Response Result is null."))
        } else {
            mappingFunction(it.result!!)
        }
    }
}


/**
 *
 */
inline fun <T, A, R> Observable<Response<T, Throwable>>.flatMapResponseWithArgumentOrErrorOut(argument : A, crossinline mappingFunction : (A, Response<T, Throwable>) -> Observable<R>) : Observable<R> {
    return this.flatMapResponseOrErrorOut { mappingFunction(argument, it) }
}


/**
 *
 */
inline fun <T, A, R> Flowable<Response<T, Throwable>>.flatMapResponseWithArgumentOrErrorOut(argument : A, crossinline mappingFunction : (A, Response<T, Throwable>) -> Flowable<R>) : Flowable<R> {
    return this.flatMapResponseOrErrorOut { mappingFunction(argument, it) }
}


/**
 *
 */
inline fun <T, A, R> Single<Response<T, Throwable>>.flatMapResponseWithArgumentOrErrorOut(argument : A, crossinline mappingFunction : (A, Response<T, Throwable>) -> Single<R>) : Single<R> {
    return this.flatMapResponseOrErrorOut { mappingFunction(argument, it) }
}


/**
 *
 */
inline fun <T, A, R> Maybe<Response<T, Throwable>>.flatMapResponseWithArgumentOrErrorOut(argument : A, crossinline mappingFunction : (A, Response<T, Throwable>) -> Maybe<R>) : Maybe<R> {
    return this.flatMapResponseOrErrorOut { mappingFunction(argument, it) }
}


/**
 *
 */
inline fun <T, R> Observable<Response<T, Throwable>>.flatMapResponseOrErrorOut(crossinline mappingFunction : (Response<T, Throwable>) -> Observable<R>) : Observable<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Observable.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Observable.error(ResponseError("The Response Result is null."))
        } else {
            mappingFunction(it)
        }
    }
}


/**
 *
 */
inline fun <T, R> Flowable<Response<T, Throwable>>.flatMapResponseOrErrorOut(crossinline mappingFunction : (Response<T, Throwable>) -> Flowable<R>) : Flowable<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Flowable.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Flowable.error(ResponseError("The Response Result is null."))
        } else {
            mappingFunction(it)
        }
    }
}


/**
 *
 */
inline fun <T, R> Single<Response<T, Throwable>>.flatMapResponseOrErrorOut(crossinline mappingFunction : (Response<T, Throwable>) -> Single<R>) : Single<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Single.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Single.error(ResponseError("The Response Result is null."))
        } else {
            mappingFunction(it)
        }
    }
}


/**
 *
 */
inline fun <T, R> Maybe<Response<T, Throwable>>.flatMapResponseOrErrorOut(crossinline mappingFunction : (Response<T, Throwable>) -> Maybe<R>) : Maybe<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Maybe.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Maybe.error(ResponseError("The Response Result is null."))
        } else {
            mappingFunction(it)
        }
    }
}


/**
 *
 */
fun <T> Observable<T>.subscribeOnIO() : Observable<T> {
    return this.subscribeOn(Schedulers.io())
}


/**
 *
 */
fun <T> Flowable<T>.subscribeOnIO() : Flowable<T> {
    return this.subscribeOn(Schedulers.io())
}


/**
 *
 */
fun <T> Single<T>.subscribeOnIO() : Single<T> {
    return this.subscribeOn(Schedulers.io())
}


/**
 *
 */
fun <T> Maybe<T>.subscribeOnIO() : Maybe<T> {
    return this.subscribeOn(Schedulers.io())
}


/**
 *
 */
fun Completable.subscribeOnIO() : Completable {
    return this.subscribeOn(Schedulers.io())
}


/**
 *
 */
fun <T> Observable<T>.observeOnUI() : Observable<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}


/**
 *
 */
fun <T> Flowable<T>.observeOnUI() : Flowable<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}


/**
 *
 */
fun <T> Single<T>.observeOnUI() : Single<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}


/**
 *
 */
fun <T> Maybe<T>.observeOnUI() : Maybe<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}


/**
 *
 */
fun Completable.observeOnUI() : Completable {
    return this.observeOn(AndroidSchedulers.mainThread())
}


/**
 *
 */
fun <T> Observable<T>.typicalBackgroundWorkSchedulers() : Observable<T> {
    return this.subscribeOnIO()
        .observeOnUI()
}


/**
 *
 */
fun <T> Flowable<T>.typicalBackgroundWorkSchedulers() : Flowable<T> {
    return this.subscribeOnIO()
        .observeOnUI()
}


/**
 *
 */
fun <T> Single<T>.typicalBackgroundWorkSchedulers() : Single<T> {
    return this.subscribeOnIO()
        .observeOnUI()
}


/**
 *
 */
fun <T> Maybe<T>.typicalBackgroundWorkSchedulers() : Maybe<T> {
    return this.subscribeOnIO()
        .observeOnUI()
}


/**
 *
 */
fun Completable.typicalBackgroundWorkSchedulers() : Completable {
    return this.subscribeOnIO()
        .observeOnUI()
}


/**
 *
 */
fun <T> T.asObservable() : Observable<T> {
    return Observable.just(this)
}


/**
 *
 */
fun <T> T.asFlowable() : Flowable<T> {
    return Flowable.just(this)
}


/**
 *
 */
fun <T> T.asSingle() : Single<T> {
    return Single.just(this)
}


/**
 *
 */
fun <T> T.asMaybe() : Maybe<T> {
    return Maybe.just(this)
}

